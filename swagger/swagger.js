
module.exports = {
    swaggerDefinition: {
        openapi: '3.0.0',
        info: {
            title: 'Swagger Project',
            description: 'This is a swagger server project. You can find out more about Swagger at [swagger.io](http://swagger.io), [swagger specification](https://swagger.io/docs/specification/about/), [basic structure openapi v3](https://swagger.io/docs/specification/basic-structure/), [openapi v3.0.0](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md).',
            version: '0.0.1'
        },
        servers: [
            {
                url: 'http://localhost:3003/api',
                description: 'Server Mock; mocked testing server.'
            },
            {
                url: 'http://localhost:3000/api',
                description: 'Express Server; api server.'
            }
        ]
    },
    apis: ['./modules/**/*.js']
}