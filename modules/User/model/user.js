
// USER MODEL

/** 
 * @swagger
 * 
 * components:
 *  schemas:
 *    User:
 *      type: object
 *      properties:
 *        id:
 *          type: string
 *          format: uuid
 *        user_name:
 *          type: string
 *        user_type:
 *          type: string
 *          format: enum
 *        is_pro:
 *          type: boolean
 *        points:
 *          type: number
 *          format: float
 *        game_genre_favorite:
 *          type: array
 *          items:
 *            type: string
 *        game_owned:
 *          type: array
 *          items:
 *            type: uuid
 *
 *    UserRequest:
 *      type: object
 *      properties:
 *        userName:
 *          type: string
 *        userType:
 *          type: string
 *          format: enum
 *        isPro:
 *          type: boolean
 *        points:
 *          type: number
 *          format: float
 *        gameGenreFavorite:
 *          type: array
 *          items:
 *            type: string
 *        gameOwned:
 *          type: array
 *          items:
 *            type: uuid
 * 
 *  securitySchemes:
 *    BearerAuth:
 *      type: http
 *      in: header
 *      scheme: bearer
 *      bearerFormat: JWT
 */

