
// USER CONTROLLER

/**
 * @swagger
 *
 * tags:
 * - name: User
 *   description: 'User operations'
 *   externalDocs:
 *      description: 'More user info'
 *      url: 'http://user.api.cc'
 * 
 */


/** 
 * @swagger
 * 
 * components:
 *  responses:
 *    UserResponseCreate:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *        id:
 *          type: string
 *          format: uuid
 *        userName:
 *          type: string
 *        userType:
 *          type: string
 *          format: enum
 * 
 *    UserResponseFetch:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *        user:
 *          type: object
 *          properties:
 *            userName:
 *              type: string
 *            userType:
 *              type: string
 *              format: enum
 *            isPro:
 *              type: boolean
 *            points:
 *              type: number
 *              format: float
 *            gameGenreFavorite:
 *              type: array
 *              items:
 *                type: string
 *            gameOwned:
 *              type: array
 *              items:
 *                type: uuid
 * 
 */