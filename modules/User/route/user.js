
// USER ROUTE


/**
 * @swagger
 *
 * paths:
 *  /user:
 *    post:
 *     tags: [User]
 *     summary: Create user
 *     description: User creation
 *     operationId: createUser    			
 *     requestBody:
 *        required: true 
 *        content:
 *          application/json:
 *            schema:
 *               $ref: '#/components/schemas/UserRequest'
 *            example:
 *              userName: breno_fighters
 *              userType: Gamer
 *              isPro: true
 *              points: 72.33
 *              gameGenreFavorite: [ fighting, action ]
 *              gameOwned: [ 5ec530a5-3261-45a4-a9e4-709990e6dc08, 47c2f8bd-f2ca-4819-a376-dff4a90a225c ]
 *     responses:
 *       '201':
 *         description: Successful operation
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UserResponseCreate'
 *             example:
 *               message: created
 *               id: "f5287575-2f2d-4b6e-8621-716abfac57c8"
 *               userName: breno_fighters
 *               userType: Gamer
 *       '400':
 *         description: Invalid parameters
 *       '500':
 *         description: Internal server error
 *
 */
// router endpoint to create user


/**
 * @swagger
 *
 * paths:
 *  /user/{id}:
 *    get:
 *     tags: [User]
 *     summary: Retrieve user by id
 *     description: User fetch by id
 *     operationId: getUserById
 *     parameters:
 *       - name: id
 *         in: path
 *         description: needs to be fetched, use user id
 *         required: true
 *         schema:
 *           type: string
 *           format: uuid
 *     responses:
 *       '200':
 *         description: Successful operation
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UserResponseFetch'
 *             example:
 *               message: ok
 *               user: {
 *                 userName: breno_fighters,
 *                 userType: Gamer,
 *                 isPro: true,
 *                 points: 72.33,
 *                 gameGenreFavorite: [ fighting, action ],
 *                 gameOwned: [ 5ec530a5-3261-45a4-a9e4-709990e6dc08, 47c2f8bd-f2ca-4819-a376-dff4a90a225c ]
 *               }
 *       '400':
 *         description: Invalid parameters
 *       '404':
 *         description: User not found
 *       '500':
 *         description: Internal server error
 *
 */
// router endpoint to fetch user by id


/**
 * @swagger
 *
 * paths:
 *  /user/{id}:
 *    put:
 *     tags: [User]
 *     summary: Update user by id
 *     description: User update by id
 *     operationId: updateUserById
 *     parameters:
 *       - name: id
 *         in: path
 *         description: needs to be fetched, use user id
 *         required: true
 *         schema:
 *           type: string
 *           format: uuid
 *     requestBody:
 *        required: true 
 *        content:
 *          application/json:
 *            schema:
 *               $ref: '#/components/schemas/UserRequest'
 *            example:
 *              userName: bruno_striker
 *              userType: Accurator 
 *              isPro: false
 *              points: 99
 *              gameGenreFavorite: [ rpg, adventure, strategy ]
 *              gameOwned: [ 5ec530a5-3261-45a4-a9e4-709990e6dc08 ]
 *     responses:
 *       '204':
 *         description: Successful operation
 *         content:
 *           application/json:
 *             example:
 *       '400':
 *         description: Invalid parameters
 *       '404':
 *         description: User not found
 *       '500':
 *         description: Internal server error
 *     security:
 *       - BearerAuth: []
 *
 */
// router endpoint to update user by id


/**
 * @swagger
 *
 * paths:
 *  /user/{id}:
 *    delete:
 *     tags: [User]
 *     summary: Delete user by id
 *     description: User delete by id
 *     operationId: deleteUserById
 *     parameters:
 *       - name: id
 *         in: path
 *         description: needs to be deleted, use user id
 *         required: true
 *         schema:
 *           type: string
 *           format: uuid
 *     responses:
 *       '204':
 *         description: Successful operation
 *         content:
 *           application/json:
 *             example:
 *       '400':
 *         description: Invalid parameters
 *       '404':
 *         description: User not found
 *       '500':
 *         description: Internal server error
 *
 */
// router endpoint to delete user by id