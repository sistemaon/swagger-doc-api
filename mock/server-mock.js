
// server.js
const jsonServer = require('json-server')
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const dbPath = './mock/db.json'
const server = jsonServer.create()
const router = jsonServer.router(dbPath)
const middlewares = jsonServer.defaults()

const adapter = new FileSync(dbPath)
const db = low(adapter)

server.use(middlewares)
server.use(jsonServer.bodyParser)

// Random create user id uuid for mocking user id purpose
const uuidv4 = () => 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
  const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8)
  return v.toString(16)
}
)

// USERS
server.post('/api/user', (req, res) => {

  const userReqBody = req.body
  const userObj = {
    id:                    uuidv4(),                        // (uuid)(String) (Not Null)         // "f5287575-2f2d-4b6e-8621-716abfac57c8"
    user_name:             userReqBody.userName,            // (String) (Not Null)               // "breno_fighters"
    user_type:             userReqBody.userType,            // (<Enumerate<String>>) (Not Null)  // "Gamer" "Accurator" "Publisher"
    is_pro:                userReqBody.isPro,               // (Boolean) (Not Null)              // true
    points:                userReqBody.points,              // (Float) (Not Null)                // 120.75
    game_genre_favorite:   userReqBody.gameGenreFavorite,   // (<Array<String>>)  (Nullable)     // ["fighting", "action"]
    game_owned:            userReqBody.gameOwned            // (<Array<uuid>>)  (Nullable)       // ["196a962d-8903-4282-ae40-6a9e54d92551", "7fb010ff-4149-4d40-a61a-e8613200d3e1"]
  }

  const newUser = db.get('user').push(userObj).write()

  const userRes = {
    message:    "created",
    id:         userObj.id,
    userName:   userObj.user_name,
    userType:   userObj.user_type
  }

  res.status(201).json(userRes)

})

server.get('/api/users', (req, res) => {

  const user = db.get('user')

  const userMap = user.map(x => {

    const userObj = {
      id:              x.id,
      userName:        x.user_name
    }

    return userObj

  })

  res.status(200).json(userMap)

})

server.get('/api/user/:identity', (req, res) => {

  const idty = req.params.identity
  const user = db.get('user').find({ id: idty }).value() 
  
  const userObj = {
    userName:             user.user_name,
    userType:             user.user_type,
    isPro:                user.is_pro,
    points:               user.points,
    gameGenreFavorite:    user.game_genre_favorite,
    gameOwned:            user.game_owned
  }

	const userRes = {
    message:    "ok",
    user:		    userObj
  }

  res.status(200).json(userRes)

})

server.put('/api/user/:identity', (req, res) => {

  const userReqBody = req.body

  const userObj = {
    user_name:              userReqBody.userName,
    user_type:              userReqBody.userType,
    is_pro:                 userReqBody.isPro,
    points:                 userReqBody.points,
    game_genre_favorite:    userReqBody.gameGenreFavorite,
    game_owned:             userReqBody.gameOwned
  }

  const idty = req.params.identity
  const user = db.get('user').find({ id: idty }).assign(userObj).write()

  res.status(204).json(user)

})

server.delete('/api/user/:identity', (req, res) => {

  const idty = req.params.identity
  const user = db.get('user').remove({ id: idty }).write()

  res.status(204).json({ message: 'User deleted!' })

})
// USERS

// GAMES
server.post('/api/game', (req, res) => {

  const gameReqBody = req.body
  const gameObj = {
    id:             uuidv4(),                   // (uuid)(String) (Not Null)          // "196a962d-8903-4282-ae40-6a9e54d92551"
    game_name:      gameReqBody.gameName,       // (String) (Not Null)                // "Street Figther"
    game_genre:     gameReqBody.gameGenre       // (<Array<String>>)  (Nullable)      // ["fighting", "rpg"]
  }

  const newGame = db.get('game').push(gameObj).write()

  const gameRes = {
    message:      "created",
    id:           gameObj.id,
    gameName:     gameObj.game_name
  }

  res.status(201).json(gameRes)

})

server.get('/api/games', (req, res) => {

  const game = db.get('game')

  const gameMap = game.map(x => {

    const gameObj = {
      id:              x.id,
      gameName:        x.game_name
    }

    return gameObj

  })

  res.status(200).json(gameMap)

})

server.get('/api/game/:identity', (req, res) => {

  const idty = req.params.identity
  const game = db.get('game').find({ id: idty }).value() 
  
  const gameObj = {
    id:             game.id,
    gameName:       game.game_name,
    gameGenre:      game.game_genre
  }

	const gameRes = {
    message:    "ok",
    game:		    gameObj
  }

  res.status(200).json(gameRes)

})

server.put('/api/game/:identity', (req, res) => {

  const gameReqBody = req.body

  const gameObj = {
    game_name:      gameReqBody.gameName,
    game_genre:     gameReqBody.gameGenre
  }

  const idty = req.params.identity
  const game = db.get('game').find({ id: idty }).assign(gameObj).write()

  res.status(204).json(game)

})

server.delete('/api/game/:identity', (req, res) => {

  const idty = req.params.identity
  const game = db.get('game').remove({ id: idty }).write()

  res.status(204).json({ message: 'Game deleted!' })

})
// GAMES





// MOCK TEST TDD
server.get('/api/mock', (req, res) => {
  res.send({ title: 'Mocked Server' });
})
// MOCK TEST TDD

server.use(router)

const port = 3003
server.listen(port, () => {
  console.log('JSON Server is running ::; ', port)
})

// Exporting server mock app
module.exports = server
